<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    function ubah_huruf($string)
    {
        $huruf = "abcdefghijklmnopqrstuvwxyz";
        $output = " ";
        for ($i = 0; $i < strlen($string); $i++) {
            $position = strpos($huruf, $string[$i]);
            $output .= substr($huruf, $position + 1, 1);
        }
        return $output . "<br>";
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>

</html>